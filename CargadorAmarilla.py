'''
fileoverview 			CargadorAmarilla.py
Este software cargar en la Micro:bit amarilla el programa recibido por MQTT
   
version                               1.0
 
author                 	Rafael Cuesta Ruiz (2021)
copyright           	Rafael Cuesta Ruiz (2021)
----
'''



import os.path,shutil,time,subprocess,commands
import paho.mqtt.client as mqtt

numMicrobits = 0


programAMicBPath = "programAFinalMicrobitB.hex"
programBMicBPath = "programBFinalMicrobitB.hex"
programCMicBPath = "programCFinalMicrobitB.hex"


pathToNodeScript = "/home/pi/Desktop/bbolw/ble-uart-to-udp-master/bbowlAmarilla.js"

pathMicrobitB = None

pathMicrobit = "/media/pi/MICROBIT"

topic = "tfg/Amarilla/rafa"
print (topic)
ipMqtt = "45.76.84.39"
port = 1883


def on_connect(client, userdata, flags, rc):
    global pathMicrobit
    print("MQTT La conexion ha ocurrido con codigo " + str(rc))
    client.subscribe(topic)
    #flasheaYLanzarNodeMicrobit_programaB(pathMicrobit)
    

def on_message(client, userdata, msg):
    programaA = programAMicBPath
    programaB = programBMicBPath
    programaC = programCMicBPath    

    if msg.payload == 'A':
        print("MQTT Lanzando programaA con Path"+pathMicrobit)
        flasheaYLanzarNodeMicrobit_programaA(pathMicrobit,programaA)
        
    elif msg.payload == 'B':
        print("MQTT Lanzando programaB"+pathMicrobit)
        flasheaYLanzarNodeMicrobit_programaB(pathMicrobit,programaB)
        
    elif msg.payload == 'C':
        print("MQTT Lanzando programaC"+pathMicrobit)
        flasheaYLanzarNodeMicrobit_programaC(pathMicrobit,programaC)
        

def flashearProgramaA(pathMicrobit,programaA):
    print("Entro en flashearProgramaA con pathMicrobit:"+pathMicrobit)  
    print("NumMicrobits en flashearProgramaA=:"+str(numMicrobits))

    rutaOrigen = programaA
    rutaDestino = os.path.join(pathMicrobit,programA)
    
    print("Voy a flashear copiando a "+rutaDestino)
    shutil.copy2(rutaOrigen,rutaDestino)


def flasheaYLanzarNodeMicrobit_programaA(pathMicrobit,programaA):
    print("Entro en flashearYLanzarNodeMicrobit_programaA con pathMicrobit:"+pathMicrobit) 
    flashearProgramaA(pathMicrobit,programaA)

    print("Esperando a que responda la microbit!")
    time.sleep(3)

    ejecutarNodeArgs = ["node", pathToNodeScript]
    subprocess.Popen(ejecutarNodeArgs)


def flashearProgramaB(pathMicrobit,programaB):
    print("Entro en flashearProgramaB con pathMicrobit:"+pathMicrobit) 
    print("NumMicrobits en flashearProgramaB=:"+str(numMicrobits))

    rutaOrigen = programaB
    rutaDestino = os.path.join(pathMicrobit,programaB)
    
    print("Voy a flashear copiando a "+rutaDestino)
    shutil.copy2(rutaOrigen,rutaDestino)


def flasheaYLanzarNodeMicrobit_programaB(pathMicrobit,programaB):
    print("Entro en flashearYLanzarNodeMicrobit_programaB con pathMicrobit:"+pathMicrobit) 
    flashearProgramaB(pathMicrobit,programaB)

    print("Esperando a que responda la microbit!")
    time.sleep(3)

    ejecutarNodeArgs = ["node", pathToNodeScript]
    subprocess.Popen(ejecutarNodeArgs)


def flashearProgramaC(pathMicrobit,programaC):
    print("Entro en flashearProgramaC con pathMicrobit=:"+pathMicrobit)
    print("NumMicrobits en flashearProgramaC=:"+str(numMicrobits))

    rutaOrigen = programaC
    rutaDestino = os.path.join(pathMicrobit,programaC)
   
    print("Voy a flashear copiando a "+rutaDestino)
    shutil.copy2(rutaOrigen,rutaDestino)

def flasheaYLanzarNodeMicrobit_programaC(pathMicrobit,programaC):
    print("Entro en flashearYLanzarNodeMicrobit_programaC con pathMicrobit=:"+pathMicrobit)  
    flashearProgramaC(pathMicrobit,programaC)

    print("Esperando a que responda la microbit!")
    time.sleep(3)

    ejecutarNodeArgs = ["node", pathToNodeScript]
    subprocess.Popen(ejecutarNodeArgs)
    
def checkearNumMicrConectadas(): 
    global numMicrobits
    global pathMicrobitB
    
    nombreMontaje = str(commands.getoutput('mount | grep /dev/sda'))
    if "MICROBIT " in nombreMontaje:
        print("Conectada Microbit en media /dev/sda con punto de montaje /media/pi/MICROBIT")
        numMicrobits = numMicrobits + 1
        #print("Incrementado el num de Microbit en uno: " + str(numMicrobits))
        pathMicrobitB = "/media/pi/MICROBIT"
        
    nombreMontaje = str(commands.getoutput('mount | grep /dev/sdb'))
    if "MICROBIT1" in nombreMontaje:
        print("Conectada Microbit en media /dev/sdb con punto de montaje /media/pi/MICROBIT1")
        numMicrobits = numMicrobits + 1
        pathMicrobitA = "/media/pi/MICROBIT1"
        print("En segundo if pathMicrobitB es: "+pathMicrobitB)
        
        
#pathMicrobitA = uflash.find_microbit()


checkearNumMicrConectadas()
print ("El numero de mbits conectadas es :" + str(numMicrobits)) 

if pathMicrobitB == None:
    print ("No se ha detectado la segunda microbit (Amarilla)")
    exit()


pAmBIsOk = os.path.exists(programAMicBPath)
print ("pAmBIsOk"+str(pAmBIsOk))
pBmBIsOk = os.path.exists(programBMicBPath)
print ("pBmBIsOk"+str(pBmBIsOk))
pCmBIsOk = os.path.exists(programCMicBPath)
print ("pCmBIsOk"+str(pCmBIsOk))


if not pAmBIsOk or not pBmBIsOk or not pCmBIsOk :
    print("No se han podido encontrar los programas a usar de la microbit!")
    exit()


if not os.path.exists(pathToNodeScript):
   print("No se ha podido encontrar el script de node!")
   exit()

print ("El valor de pathMicrobitB es:"+str(pathMicrobitB))


client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect(ipMqtt, port, 300)

client.loop_forever()
