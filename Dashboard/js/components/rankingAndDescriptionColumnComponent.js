/**
 * @fileoverview 	rankingAndDescriptionColumnComponent.js
 *	Este componente representa la columna de descripción + ranking, permite reutilizar marcado/html
 *  
 * @version                               1.0
 *
 * @author                 	Rafael Cuesta Ruiz (2021)
 * @copyright           	Rafael Cuesta Ruiz (2021)
 * ----

*/


ko.components.register('columninfo', {
    viewModel: function(params) {

        this.description = params.description;

        this.ranking = params.ranking;

        this.maxLines = params.maxLines;

    },
    template:
        '<div class="mt-4">\
        <h1>Ranking</h1>\
        <table class="table table-striped table-dark">\
          <thead>\
            <tr>\
              <th scope="col">#</th>\
              <th scope="col">Nombre</th>\
              <th scope="col">Tiempo en conseguido</th>\
              <th scope="col">Puntos</th>\
            </tr>\
          </thead>\
          <tbody data-bind="foreach: ranking().slice(0,maxLines())">\
            <tr>\
              <th scope="row" data-bind="text: $index()+1"></th>\
              <td data-bind="text: $data.getName()"></td>\
              <td data-bind="text: $data.getTimeCompleted()"></td>\
              <td data-bind="text: $data.getPoints()"></td>\
            </tr>\
          </tbody>\
        </table>\
        \
        <h1 class="titleReto">El reto consisten en...</h1>\
        <p class="descriptionReto" data-bind="html:description">\
        </p>\
       </div>'
});
