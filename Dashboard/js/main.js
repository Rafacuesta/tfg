/**
 * @fileoverview 	main.js
 *	Instancio por primera vez todos mis servicios.
 *
 * @version                               1.0
 *
 * @author                 	Rafael Cuesta Ruiz (2021)
 * @copyright           	Rafael Cuesta Ruiz (2021)
 * ----

*/


var storage = new programsStorage();
var configStorage = new configStorage();

var myRankingService = new rankingService(storage);
var myConfigService = new configService(configStorage);

var challengeLetrasService = new challengeLetrasService(myConfigService);
var challengePosicionesService = new challengePosicionesService(myConfigService);
var challengeTemperaturaService = new challengeTemperaturaService(myConfigService);

var mqttService = new mqttService(challengeLetrasService, challengePosicionesService, challengeTemperaturaService);

// Me conecto al servidor mqtt
mqttService.connect();


// Inicializo el view model principal con todos los servicios construidos
var viewModel = new masterViewModel(myRankingService, myConfigService,
  challengeLetrasService,challengePosicionesService, challengeTemperaturaService, mqttService);


// Le decimos a knockout que aplique los bindings
ko.applyBindings(viewModel);

//Llmamos a nuestro metodo iniciar del master view model
viewModel.init();
