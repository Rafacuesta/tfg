/**
 * @fileoverview 	configStorage.js
 *	Este componente funciona como un repositorio y permite cargar y guardar una configuración en el localStorage
 *  gracias al loadConfig y al saveConfig
 * @version                               1.0
 *
 * @author                 	Rafael Cuesta Ruiz (2021)
 * @copyright           	Rafael Cuesta Ruiz (2021)
 * ----

*/


var configStorage = function(){

  var self = this;

  self.keyConfig = "config";
  self.defaultJugadorA = "Jugador A";
  self.defaultJugadorB = "Jugador B";

  /* Guarda una configuración en el localstorage*/
  self.saveConfig = function(config){

    var jsonConfig = JSON.stringify(config);
    localStorage.setItem(self.keyConfig, jsonConfig);


  }

  /*Carga una configuración del localstorage*/
  self.loadConfig = function(){

    var jsonConfigRaw = localStorage.getItem(self.keyConfig);

    if(jsonConfigRaw == null){
      return new config(self.defaultJugadorA,self.defaultJugadorB);
    }

    jsonConfig = JSON.parse(jsonConfigRaw)

    return new config(jsonConfig.nameUserA, jsonConfig.nameUserB);
  }


}
