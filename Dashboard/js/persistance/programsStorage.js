/**
 * @fileoverview 	programsStorage.js
 *	Este componente funciona como un repositorio y nos permite cargar un ranking, guardar un ranking y borrarlo si necesitamos
 *  internamente almacenar todo esto en un localstorage de javascript
 *
 * @version                               1.0
 *
 * @author                 	Rafael Cuesta Ruiz (2021)
 * @copyright           	Rafael Cuesta Ruiz (2021)
 * ----

*/

var programsStorage = function(){

  var self = this;


/* Carga un raking del localStorage*/
  self.loadRankingProgram = function(nameProgram){

     var loadedRanking = new ranking();
     var jsonLines = localStorage.getItem(nameProgram);
     var lines = jsonLines===null?[]:JSON.parse(jsonLines);

     for(var index in lines) {
       loadedRanking.addRankingLine(new rankingLine(lines[index].name, lines[index].timeCompletedInSeconds));
     }

     return loadedRanking;
  }


/* Guarda un ranking en el localStorage */
  self.saveRankingProgram = function(nameProgram, ranking){

     var jsonLines = JSON.stringify(ranking.lines);
     localStorage.setItem(nameProgram, jsonLines);

  }


/* Borra un ranking del localStorage */
  self.deleteRankingProgram = function(nameProgram){
      localStorage.removeItem(nameProgram);
  }



}
