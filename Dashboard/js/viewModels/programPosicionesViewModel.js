/**
 * @fileoverview 	programPosicionesViewModel.js
 *	Este view model invoca toda la lógica de negocio necesaria para el programa de las posiciones
 *
 * @version                               1.0
 *
 * @author                 	Rafael Cuesta Ruiz (2021)
 * @copyright           	Rafael Cuesta Ruiz (2021)
 * ----

*/


var programPosicionesViewModel = function(rankingService,configService, challengePosicionesService)
{

    var self = this;


   /* Inicializamos nuestros servicios*/
    self.rankingService = rankingService;
    self.configService = configService;
    self.challengePosicionesService = challengePosicionesService;


    /*Observables a los nombres de nuestros jugadores*/
    self.nameJugadorA = ko.observable("");
    self.nameJugadorB = ko.observable("");

    self.timeRemaining = ko.observable("?");

     /* Este observabe tiene y sale inicializado con la descripción del reto actual */
    self.descriptionReto = ko.observable(`
      <p>El reto consiste en conseguir rotar la micro:bit para conseguir varias posiciones :
      <ul>
        <li>Agitar la micro:bit</li>
        <li>Caída libre</li>
        <li>	Boca arriba – Boca abajo</li>
      </ul>
    </p>

<p>Dispones de un tiempo de 5 minutos.
Una vez iniciada, pulsa los botones A y B de forma simultánea para marcar la posición.</p>

<p>En la parte superior verás el ranking de las puntuaciones. En la pantalla de la derecha verás la interacción de dos microbits.
</p>

Puedes ir a la página de configuración para realizar la carga de la microbit correspondiente al programa Posiciones.</p>
`);

     /* Observables para el ranking actual del programa*/
    self.ranking = ko.observable(new ranking());
    self.maxLinesRanking = ko.observable(5);



    /* Cargamos la configuración actual (ranking y nombre de jugadores) */
    self.init = function(){
       self.loadRanking();
       self.loadCurrentUsers();
    }


     /* Iniciamos el reto actual */
    self.initChallenge = function(){

        self.challengePosicionesService.initChallenge(self.updateRemainingTime, self.addNewLineRanking);

    }

    /* Este método se invoca cuando el tiempo restante del rento se ha actualizado*/
    self.updateRemainingTime = function(timeLeft){
      self.timeRemaining(timeLeft);
    }

    /* Carga el nombre de los jugadores actual */
    self.loadCurrentUsers = function(){

      self.nameJugadorA(self.configService.getNameUserA());
      self.nameJugadorB(self.configService.getNameUserB());

    }


   /* Este método carga el ranking actual*/
    self.loadRanking = function(){
        var ranking = self.rankingService.loadRankingPosiciones();
        self.ranking(ranking);
    }


    /* Este método persiste/guarda el ranking actual*/
    self.saveRanking = function(){
      self.rankingService.saveRankingPosiciones(self.ranking());
    }


    /* Este método se invoca cuando se añade una nueva línea de puntuación al ranking*/
    self.addNewLineRanking = function(name, timeCompleted){

      var timeCompletedAux;

      if(!timeCompleted){
        timeCompletedAux = null;
      }
      else{
        timeCompletedAux=timeCompleted;
      }

      var newLine = new rankingLine(name, timeCompletedAux);
      self.ranking().addRankingLine(newLine);

      self.saveRanking();
      self.loadRanking();

    }


     /* Este método borra el ranking actual*/
    self.resetRanking = function(){

      if(!confirm("¿Esta seguro que desea reiniciar el ránking del programa letras?"))
          return;


      self.rankingService.resetRankingPosiciones();
      self.loadRanking();
    }

};
