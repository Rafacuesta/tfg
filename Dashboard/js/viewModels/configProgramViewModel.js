/**
 * @fileoverview 	configProgramViewModel.js
 *	Este viewmodel invoca a la lógica de negocio necesaria para la configuración.
 *
 * @version                               1.0
 *
 * @author                 	Rafael Cuesta Ruiz (2021)
 * @copyright           	Rafael Cuesta Ruiz (2021)
 * ----

*/


var configProgramViewModel = function(configService , masterViewModel, mqttService){

   var self = this;

   // Nuestras dos propiedades observables para leer y actualizar los datos de los jugadoresA.
   self.nameUserA = ko.observable();
   self.nameUserB = ko.observable();

   // Referencias a servicios
   self.masterViewModel = masterViewModel;
   self.mqttService = mqttService;

   // Un observable para reflejar el estado de conexion al servidor mqtt
   self.isConnectedMqtt = ko.observable(false);

   // Nuestras propiedades observables para los estados actuales de los programas de cada jugador.
   self.currentProgramA = ko.observable("Desconocido");
   self.currentProgramB = ko.observable("Desconocido");

    // Otro observable para seleccionar el programa actual que deseas cargar
   self.listaProgramas = ko.observableArray(["Letras","Temperatura","Posiciones"]);
   // En este observable almacenamos el programo actual que se desea cargar
   self.selectedProgram = ko.observable();


   /* Funcion de inicio, carga desde el servicio de configuración el nombre de los jugadores actuales.
      Le paso unas callbacks a mqttService para ser notificado cuando me conecte/desconecte al servidorMQTT o
      cuando cambie uno de los programas */

   self.init = function(){

      self.nameUserA(configService.getNameUserA());
      self.nameUserB(configService.getNameUserB());
      self.mqttService.setCallBacks(self.updateMqttConnected,self.updateNameCurrentProgramA, self.updateNameCurrentProgramB);

   }



  /* Este metodo se invocara cuando la conexión al servidor mqtt cambie */
   self.updateMqttConnected = function(state){
     self.isConnectedMqtt(state);
   }

   /* Este método se invocara cuando el jugadorA cambie de programa actual */
   self.updateNameCurrentProgramA = function(nameProgram){
     self.currentProgramA(nameProgram);
   }

   /* Este método se invocara cuando el jugadorB cambie de programa actual */
   self.updateNameCurrentProgramB = function(nameProgram){
     self.currentProgramB(nameProgram);
   }

   /* Este método de vuelve un true si la cadena de texto esta vacía*/
   self.invalidName = function(name){
      return (name.length === 0 || !name.trim());
   }

  // Este método se invoca cuando se pulsa en el botón guardar nombres.
   self.guardarNombres = function(){

     var nameUserA = self.nameUserA();
     var nameUserB = self.nameUserB();

     if(self.invalidName(nameUserA) || self.invalidName(nameUserB))
     {
       alert("Uno o varios de los nombres no son válidos. Revíselo y vuelva a intentarlo :(");
       return;
     }

      configService.saveNameUsers(nameUserA, nameUserB);

      self.masterViewModel.init();

   }


  // Este método se invoca cuando se pulsa en el boton cargar programa, y carga el programa adecuado.
   self.cargarPrograma = function(){

     var selected = self.selectedProgram();

     if(selected === "Letras"){
        self.mqttService.sendMessage("A");
     }

     else if(selected === "Temperatura"){
        self.mqttService.sendMessage("B");
     }

     else if(selected === "Posiciones"){
        self.mqttService.sendMessage("C");
     }

   }


}
