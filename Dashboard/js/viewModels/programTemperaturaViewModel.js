/**
 * @fileoverview 	programTemperaturaViewModel.js
 *	Este view model invoca toda la lógica de negocio necesaria para el programa de la temperatura
 *
 * @version                               1.0
 *
 * @author                 	Rafael Cuesta Ruiz (2021)
 * @copyright           	Rafael Cuesta Ruiz (2021)
 * ----

*/

var programTemperaturaViewModel = function(rankingService, configService, challengeTemperaturaService)
{

    var self = this;

    /* Inicializamos nuestros servicios*/
    self.rankingService = rankingService;
    self.challengeTemperaturaService = challengeTemperaturaService;
    self.configService = configService;


    /*Observables a los nombres de nuestros jugadores*/
    self.nameJugadorA = ko.observable("");
    self.nameJugadorB = ko.observable("");

    /* Observable que controla el tiempo restante del reto*/
    self.timeRemaining = ko.observable("?");

    self.firstTemperaturaUserA = ko.observable("?");
    self.firstTemperaturaUserB = ko.observable("?");

   /* Este observabe tiene y sale inicializado con la descripción del reto actual */
    self.descriptionReto = ko.observable(`<p>¿Preparado para subir la temperatura?</p>
<p>Las reglas de este juego son sencillas.</p>
<p>Una vez iniciada, la micro:bit mostrará la temperatura ambiente que te servirá como punto de partida.
El reto consiste en subir la temperatura de la micro:bit en tres grados. Dispones de cinco minutos para lograrlo.</p>

<p>En la parte superior verás el ranking de las puntuaciones.
En la pantalla de la derecha verás la interacción de dos microbits.</p>
Puedes ir a la página de configuración para realizar la carga de la microbit correspondiente al programa Temperatura.
`);


     /* Observables para el ranking actual del programa*/
    self.ranking = ko.observable(new ranking());
    self.maxLinesRanking = ko.observable(5);


    /* Cargamos la configuración actual (ranking y nombre de jugadores) */
    self.init = function(){
      self.loadRanking();
      self.loadCurrentUsers();
    }

    /* Iniciamos el reto actual */
    self.initChallenge = function(){

      self.challengeTemperaturaService.initChallenge(self.updateRemainingTime,
        self.addNewLineRanking, self.updateFirstTemperaturaUserA, self.updateFirstTemperaturaUserB);


    }

     /* Este método se actualiza cuando llega la primera tempratura del jugadorA */

    self.updateFirstTemperaturaUserA = function(firstTemperaturaA){
        self.firstTemperaturaUserA(firstTemperaturaA);
    }

    /* Este método se actualiza cuando llega la primera tempratura del jugadorB */
    self.updateFirstTemperaturaUserB = function(firstTemperaturaB){
        self.firstTemperaturaUserB(firstTemperaturaB);
    }


    /* Este método se invoca cuando el tiempo restante del rento se ha actualizado*/
    self.updateRemainingTime = function(timeLeft){
      self.timeRemaining(timeLeft);
    }


    /* Carga el nombre de los jugadores actual */
    self.loadCurrentUsers = function(){

      self.nameJugadorA(self.configService.getNameUserA());
      self.nameJugadorB(self.configService.getNameUserB());

    }


    /* Este método carga el ranking actual*/
    self.loadRanking = function(){
        var ranking = self.rankingService.loadRankingTemperatura();
        self.ranking(ranking);
    }

    /* Este método persiste/guarda el ranking actual*/
    self.saveRanking = function(){
      self.rankingService.saveRankingTemperatura(self.ranking());
    }

     /* Este método se invoca cuando se añàde una nueva linea de puntuación al ranking*/

    self.addNewLineRanking = function(name, timeCompleted){

      var timeCompletedAux;

      if(!timeCompleted){
        timeCompletedAux = null;
      }
      else{
        timeCompletedAux=timeCompleted;
      }

      var newLine = new rankingLine(name, timeCompletedAux);
      self.ranking().addRankingLine(newLine);

      self.saveRanking();
      self.loadRanking();

    }


     /* Este método borra el ranking actual*/
    self.resetRanking = function(){

        if(!confirm("¿Esta seguro que desea reiniciar el ránking del programa temperatura?"))
            return;

        self.rankingService.resetRankingTemperatura();
        self.loadRanking();
    }




};
