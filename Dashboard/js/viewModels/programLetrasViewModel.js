/**
 * @fileoverview 	programLetrasViewModel.js
 *	Este view model invoca toda la lógica de negocio necesaria para el programa de las letras.
 *
 * @version                               1.0
 *
 * @author                 	Rafael Cuesta Ruiz (2021)
 * @copyright           	Rafael Cuesta Ruiz (2021)
 * ----

*/


var programLetrasViewModel = function(rankingService, configService, challengeLetrasService)
{

    var self = this;


    /* Inicializamos nuestros servicios*/
    self.rankingService = rankingService;
    self.configService = configService;
    self.challengeLetrasService = challengeLetrasService;


    /*Observables a los nombres de nuestros jugadores*/
    self.nameJugadorA = ko.observable("");
    self.nameJugadorB = ko.observable("");

    /* Observables a la última letra que se equivocaron los jugadores*/
    self.lastLetterFailedA = ko.observable("?");
    self.lastLetterFailedB = ko.observable("?");

    /* Observable que controla el tiempo restante del reto*/
    self.timeRemaining = ko.observable("?");


    /* Este observabe tiene y sale inicializado con la descripción del reto actual */
    self.descriptionReto = ko.observable(`
    <p>El reto al que te enfrentas consiste en identificar si la letra que aparece en la Micro:bit es una vocal o una consonante. Cada 20 preguntas seguidas acertadas se consigue subir de nivel. Alcanzando el tercer nivel se logra cumplir el reto..</p>

    <p>Pulsa el botón A de la micro:bit si es una vocal o el botón B si es una consonante. Dispones de dos segundos para responder.</p>

    <p>Las reglas de puntuación son las siguientes:<br>

      <ul>
        <li>Cada vez que aciertas se consigue un punto y aparece otra letra por pantalla (que podría repetirse). El programa hace un receso cada 20 puntos indicando que se accede a un nuevo nivel (se comienza 0). Subir de nivel implica que el tiempo de respuesta se reduce 200 ms.</li>
        <li>Si te equivocas o no respondes en tiempo se mostrará por pantalla el mensaje “Game Over” indicando que el juego ha acabado. A continuación verás un mensaje con puntuación conseguida “Score = x” y el nivel alcanzado.</li>
      </ul>
  	</p>

    <p>En la parte superior verás el ranking de las puntuaciones. En la pantalla de la derecha verás la interacción de dos Micro:bits.</p>
    <p>Puedes ir a la página de configuración para realizar la carga de la microbit correspondiente al programa Letras.</p>
    `);

     /* Observables para el ranking actual del programa*/
    self.ranking = ko.observable(new ranking());
    self.maxLinesRanking = ko.observable(5);

    /* Cargamos la configuración actual (ranking y nombre de jugadores) */
    self.init = function(){

       self.loadRanking();
       self.loadCurrentUsers();
    }

    /* Carga el nombre actual de los jugadores */
    self.loadCurrentUsers = function(){

      self.nameJugadorA(self.configService.getNameUserA());
      self.nameJugadorB(self.configService.getNameUserB());

    }


     /* Iniciamos el reto actual */
    self.initChallenge = function(){
          self.challengeLetrasService.initChallenge(self.updateRemainingTime,
            self.addNewLineRanking, self.updateLastFailedLetterA, self.updateLastFailedLetterB);
    }


    /* Este método se invoca cuando el jugador A se equivocó en una letra*/

    self.updateLastFailedLetterA = function(lastFailedA){
      self.lastLetterFailedA(lastFailedA);
    }

    /* Este método se invoca cuando el jugador B se equivocó en una letra*/
    self.updateLastFailedLetterB = function(lastFailedB){
      self.lastLetterFailedB(lastFailedB);
    }

    /* Este método se invoca cuando el tiempo restante del rento se ha actualizado*/
    self.updateRemainingTime = function(timeLeft){
      self.timeRemaining(timeLeft);
    }


    /* Este método carga el ranking actual*/
    self.loadRanking = function(){
        var ranking = self.rankingService.loadRankingLetras();
        self.ranking(ranking);
    }

    /* Este método persiste/guarda el ranking actual*/
    self.saveRanking = function(){
      self.rankingService.saveRankingLetras(self.ranking());
    }


    /* Este método se invoca cuando se añàde una nueva linea de puntuación al ranking*/
    self.addNewLineRanking = function(name, timeCompleted){

      var timeCompletedAux;

      if(!timeCompleted){
        timeCompletedAux = null;
      }
      else{
        timeCompletedAux=timeCompleted;
      }

      var newLine = new rankingLine(name, timeCompletedAux);
      self.ranking().addRankingLine(newLine);

      self.saveRanking();
      self.loadRanking();

    }

    /* Este método borra el ranking actual*/
    self.resetRanking = function(){

        if(!confirm("¿Esta seguro que desea reiniciar el ránking del programa letras?"))
            return;

        self.rankingService.resetRankingLetras();
        self.loadRanking();

    }


};
