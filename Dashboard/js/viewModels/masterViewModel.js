/**
 * @fileoverview 	masterViewModel.js
 *	Este viewmodel coordina todos los viewmodels y muestra o oculta la página actual.
 *
 * @version                               1.0
 *
 * @author                 	Rafael Cuesta Ruiz (2021)
 * @copyright           	Rafael Cuesta Ruiz (2021)
 * ----

*/


var masterViewModel = function(rankingService, configService,
  challengeLetrasService, challengePosicionesService, challengeTemperaturaService ,mqttService) {

    var self = this;

    // Referencias a todos nuestros servicios necesarios
    self.rankingService = rankingService;
    self.configService = configService;

    self.challengeLetrasService = challengeLetrasService;
    self.challengePosicionesService = challengePosicionesService;
    self.challengeTemperaturaService = challengeTemperaturaService;
    self.mqttService = mqttService;

    // Este observable contiene el índice del div a mostrar
    self.currentIndex = ko.observable(0);

    // Inyectamos los servicios necesarios
    self.programLetras = new programLetrasViewModel(self.rankingService, self.configService, self.challengeLetrasService);
    self.programTemperatura = new programTemperaturaViewModel(self.rankingService, self.configService, self.challengeTemperaturaService);
    self.programPosiciones = new programPosicionesViewModel(self.rankingService, self.configService, self.challengePosicionesService);

    self.configPrograms = new configProgramViewModel(self.configService, self, self.mqttService);


    /* Este método inicializa todos los viewmodels*/
    self.init = function(){

        self.programLetras.init();
        self.programTemperatura.init();
        self.programPosiciones.init();
        self.configPrograms.init();

    }
        /* este método nos lleva a la pagina del programa letras*/
		self.irAProgramaLetras = function(){
			self.currentIndex(0);
		}

       /* este método nos lleva a la pagina de la temperatura*/
		self.irAProgramaTemperatura = function(){
			self.currentIndex(1);
		}


        /* este método nos lleva a la pagina de las posiciones*/
		self.irAProgramaPosiciones = function(){
			self.currentIndex(2);
		}
        /* este método nos lleva a la pagina de configuración de dispositivos*/
		self.irADispositivos = function(){
			self.currentIndex(3);
		}

    /* este método nos lleva a la pagina de configuración*/
    self.irAConfiguracion = function(){
      self.currentIndex(4);
    }

};
