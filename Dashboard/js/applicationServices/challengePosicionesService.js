/**
 * @fileoverview challengePosicionesService.js
 * Este servicio de retos, contiene toda la lógica de negocio necesaria para el reto de las posiciones
 *
 * @version                               1.0
 *
 * @author                 	Rafael Cuesta Ruiz (2021)
 * @copyright           	Rafael Cuesta Ruiz (2021)
 * ----

*/


var challengePosicionesService = function(configService)
{

  var self=this;

  self.configService = configService;

  self.idCanvasUserA ='canvasMicrobitAPosiciones';
  self.idCanvasUserB = 'canvasMicrobitBPosiciones';

  self.canvasUserA = document.getElementById(self.idCanvasUserA);
  self.canvasUserB = document.getElementById(self.idCanvasUserB);

  self.elapsedTime = 0;
  self.maxTime = parameters.secondsChallenge;

  self.tickTimer = null;

  self.callBackTimeLeft = null;
  self.callBackRankingAdd = null;

  self.isCompletedChallengeUserA = false;
  self.isCompletedChallengeUserB = false;


  /* Usuario A*/


  /* Los miniretos que ha cumplido el jugadorA*/
  self.freeFallUserA = false;
  self.shakeUserA = false;
  self.upUserA =  false;
  self.downUserA = false;

 /* Los miniretos que ha cumplido el jugadorB*/
  self.freeFallUserB = false;
  self.shakeUserB = false;
  self.upUserB =  false;
  self.downUserB = false;


/* Reiniciamos los miniretos del jugadorA */
  self.resetUserA = function()
  {
    self.freeFallUserA = false;
    self.shakeUserA = false;
    self.upUserA =  false;
    self.downUserA = false;
  }

/* Reiniciamos los miniretos del jugadorB */
  self.resetUserB = function()
  {
    self.freeFallUserB = false;
    self.shakeUserB = false;
    self.upUserB =  false;
    self.downUserB = false;
  }

/* Devolvemos si el jugadorA ha completado todos los miniretos*/
  self.isCompletedUserA = function()
  {

    return self.freeFallUserA && self.shakeUserA && self.upUserA && self.downUserA;

  }

/* Devolvemos si el jugadorB ha completado todos los miniretos*/
  self.isCompletedUserB = function()
  {

    return self.freeFallUserB && self.shakeUserB && self.upUserB && self.downUserB;

  }

/* Este metodo inicita el reto, tiene unos "callbacks" para detectar cuando ocurren eventos, como que avance el tiempo,
un jugador termine, o se equivoque
*/
  self.initChallenge = function(callBackTimeLeft, callBackRankingAdd)
  {

     self.callBackTimeLeft = callBackTimeLeft;
     self.callBackRankingAdd = callBackRankingAdd;


     self.isCompletedChallengeUserA = false;
     self.isCompletedChallengeUserB = false;

     self.resetUserA();
     self.resetUserB();

     self.startTimer();
     self.clearCanvasUserA();
     self.clearCanvasUserB();

  }


/* Este tick se ejecuta cada segundo y disminuye la cuenta atras, y si ha expirado el tiempo maximo, dara por perdida la partida
*/
  self.tick = function()
  {

    self.elapsedTime++;

    if(self.elapsedTime > self.maxTime){

    if(!self.isCompletedChallengeUserA)
      self.uncompletedChallengeUserA();

    if(!self.isCompletedChallengeUserB)
      self.uncompletedChallengeUserB();

    self.clearTimer();
    return;
  }

    var remainingSeconds = self.maxTime -self.elapsedTime;
    var timeLeft = self.getRemainingTime(remainingSeconds);

    if(self.callBackTimeLeft)
      self.callBackTimeLeft(timeLeft);

  }

/* Este metodo nos devuelve el tiempo restante que queda en minutos y segundos mm:ss */

  self.getRemainingTime = function(remainingSeconds){


      minutes = Math.floor(remainingSeconds / 60);
      seconds = remainingSeconds - minutes * 60;

      var finalTime = self.getTwoDigits(minutes) + ':' + self.getTwoDigits(seconds);

      return finalTime;

  }

/* Este metodo convierte un numero de un digito a dos, por ejemplo un 1 se devuelve como 01*/
  self.getTwoDigits = function (unit){

    return (unit < 10?'0':'') + unit;
  }

/* Aqui se inicia el timer, para que se invoque el tick cada segundo (1000ms)*/
  self.startTimer = function(){

     self.elapsedTime = 0;
     self.clearTimer();
     self.tickTimer = setInterval(self.tick, 1000);

  }

/* Reseteamos el timer para que no se invoque más la cuenta atras */
  self.clearTimer = function(){
    if(self.tickTimer){
       console.log("He limpiado timer!");
       clearInterval(self.tickTimer);
    }

  }

/* Limpiamos el lienzo del jugador A */
  self.clearCanvasUserA = function(){

    var context = self.canvasUserA.getContext('2d');
    context.clearRect(0,0, self.canvasUserA.width, self.canvasUserA.width);
  }

/* Limpiamos el liezno del jugador B */
  self.clearCanvasUserB = function(){

    var context = self.canvasUserB.getContext('2d');
    context.clearRect(0,0, self.canvasUserB.width, self.canvasUserB.width);

  }


  /* Notificamos en pantalla que el jugador A termino y lo volcamos en el ranking*/
  self.completedChallengeUserA = function(){

      self.drawCompletedChallengeCanvasUserA();
      self.isCompletedChallengeUserA = true;

      if(self.callBackRankingAdd)
        self.callBackRankingAdd(self.configService.getNameUserA(),self.elapsedTime);


  }

  /* Notificamos en pantalla que el jugador B termino y lo volcamos en el ranking*/

  self.completedChallengeUserB = function(){

    self.drawCompletedChallengeCanvasUserB();
    self.isCompletedChallengeUserB = true;

    if(self.callBackRankingAdd)
      self.callBackRankingAdd(self.configService.getNameUserB(),self.elapsedTime);

  }


 /* Notificamos que el jugadorA no ha completado el reto y lo volcamos en el ranking */

  self.uncompletedChallengeUserA = function(){

      self.drawUncompletedChallengeCanvasUserA();

      if(self.callBackRankingAdd)
        self.callBackRankingAdd(self.configService.getNameUserA(),null);

  }

 /* Notificamos que el jugadorB no ha completado el reto y lo volcamos en el ranking */

  self.uncompletedChallengeUserB = function(){

    self.drawUncompletedChallengeCanvasUserB();

    if(self.callBackRankingAdd)
      self.callBackRankingAdd(self.configService.getNameUserB(),null);

  }


/* Pintamos en pantalla que el jugadorA no ha completado el reto*/

  self.drawUncompletedChallengeCanvasUserA = function(){
      self.clearCanvasUserA();
      self.writeCenterTextCanvasUserA("Reto incompleto :(");
  }


/* Pintamos en pantalla que el jugadoB no ha completado el reto*/

  self.drawUncompletedChallengeCanvasUserB = function(){
    self.clearCanvasUserB();
    self.writeCenterTextCanvasUserB("Reto incompleto :(");
  }


/* Pintamos en pantalla que el jugadorA ha completado el reto*/

  self.drawCompletedChallengeCanvasUserA = function(){
      self.clearCanvasUserA();
      self.writeCenterTextCanvasUserA("¡Reto conseguido!");
  }


/* Pintamos en pantalla que el jugadorB ha completado el reto*/
  self.drawCompletedChallengeCanvasUserB = function(){
    self.clearCanvasUserB();
    self.writeCenterTextCanvasUserB("¡Reto conseguido!");
  }


/* Escribimos un texto en el centro del liezno del jugadorA */
  self.writeCenterTextCanvasUserA = function(text){

    var context = self.canvasUserA.getContext('2d');
    context.fillStyle = "purple";
    context.font = "50px Arial";
    context.textAlign = "center";
    context.fillText(text,self.canvasUserB.width/2,self.canvasUserB.height/2);

  }

/* Escribimos un texto en el centro del liezno del jugadorB */
  self.writeCenterTextCanvasUserB = function(text){

    var context = self.canvasUserB.getContext('2d');
    context.fillStyle = "blue";
    context.font = "50px Arial";
    context.textAlign = "center";
    context.fillText(text,self.canvasUserB.width/2,self.canvasUserB.height/2);


  }


  /* Cuando recibimos un mensaje, lo procesamos como el jugadorA o el jugador B */

  self.onMessage = function(nameUser, message)
  {
    if(nameUser=="A")
    {
      self.processMessageA(message);
    }

    else if(nameUser=="B")
    {
      self.processMessageB(message);
    }

  }

/* Invoca a los metodos adecuados del jugadorA */
  self.processMessageA = function(message)
  {

    if(message == "G5")
    {
        self.clearCanvasUserA();
        self.writeCenterTextCanvasUserA("BOCA ARRIBA");
        self.upUserA = true;
    }

    else if(message == "G6")
    {
      self.clearCanvasUserA();
      self.writeCenterTextCanvasUserA("BOCA ABAJO");
      self.downUserA = true;
    }

    else if(message == "G7")
    {
      self.clearCanvasUserA();
      self.writeCenterTextCanvasUserA("CAIDA");
      self.freeFallUserA = true;
    }

    else if(message == "G11")
    {
      self.clearCanvasUserA();
      self.writeCenterTextCanvasUserA("AGITADA");
      self.shakeUserA = true;
    }

    if(self.isCompletedUserA() && self.tickTimer && !self.isCompletedChallengeUserA)
    {

      self.completedChallengeUserA();
      return;

    }


  }

/* Invoca a los metodos adecuados del jugadorB */
  self.processMessageB = function(message)
  {


    if(message == "G5")
    {
        self.clearCanvasUserB();
        self.writeCenterTextCanvasUserB("BOCA ARRIBA");
        self.upUserB = true;
    }

    else if(message == "G6")
    {
      self.clearCanvasUserB();
      self.writeCenterTextCanvasUserB("BOCA ABAJO");
      self.downUserB = true;
    }

    else if(message == "G7")
    {
      self.clearCanvasUserB();
      self.writeCenterTextCanvasUserB("CAIDA");
      self.freeFallUserB = true;
    }

    else if(message == "G11")
    {
      self.clearCanvasUserB();
      self.writeCenterTextCanvasUserB("AGITADA");
      self.shakeUserB = true;
    }


      if(self.isCompletedUserB() && self.tickTimer && !self.isCompletedChallengeUserB)
      {

        self.completedChallengeUserB();
        return;

      }


  }



}
