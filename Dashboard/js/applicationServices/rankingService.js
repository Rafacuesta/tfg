/**
 * @fileoverview 	rankingService.js
 *	Este servicio consume un programsStorage, el cual es un repositorio que persiste en el localStorage.
 *  También nos permite guardar los rankings de los programas.
 *
 * @version                               1.0
 *
 * @author                 	Rafael Cuesta Ruiz (2021)
 * @copyright           	Rafael Cuesta Ruiz (2021)
 * ----

*/


var rankingService = function(programsStorage){


  var self = this;

  var keyProgramLetras = "programLetras";
  var keyProgramPosiciones = "programPosiciones";
  var keyProgramTemperatura = "programTemperatura";

  self.programsStorage = programsStorage;


  /* Este método devuelve el ranking de letras actual */
  self.loadRankingLetras = function(){
      return self.programsStorage.loadRankingProgram(keyProgramLetras);
  }

  /* Este método devuelve el ranking de posiciones actual */

  self.loadRankingPosiciones = function(){
      return self.programsStorage.loadRankingProgram(keyProgramPosiciones);
  }

  /* Este método devuelve el ranking de temperatura */

  self.loadRankingTemperatura = function(){
      return self.programsStorage.loadRankingProgram(keyProgramTemperatura);
  }


  /* Este método guarda el ranking de letras*/
  self.saveRankingLetras = function(ranking){
      self.programsStorage.saveRankingProgram(keyProgramLetras,ranking);
  }

  /* Este método guarda el ranking de posiciones */

  self.saveRankingPosiciones = function(ranking){
      self.programsStorage.saveRankingProgram(keyProgramPosiciones,ranking);
  }

 /* Este método guarda el ranking de temperaturas */
  self.saveRankingTemperatura = function(ranking){
      self.programsStorage.saveRankingProgram(keyProgramTemperatura,ranking);
  }



  /* Este método borra el ranking de letras */
  self.resetRankingLetras = function(){
    self.programsStorage.deleteRankingProgram(keyProgramLetras);
  }


  /* Este método borra el ranking de posiciones */
  self.resetRankingPosiciones = function(){
    self.programsStorage.deleteRankingProgram(keyProgramPosiciones);
  }


  /* Este método borra el ranking de temperatura */
  self.resetRankingTemperatura = function(){
    self.programsStorage.deleteRankingProgram(keyProgramTemperatura);
  }



}
