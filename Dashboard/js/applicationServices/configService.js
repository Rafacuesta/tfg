/**
 * @fileoverview 	configService.js
 *	Este servicio consume un configStorage, el cual es un repositorio que persiste en el localStorage.
 *  También nos permite guardar el nombre de los jugadores y obtenerlos para su consulta
 *
 * @version                               1.0
 *
 * @author                 	Rafael Cuesta Ruiz (2021)
 * @copyright           	Rafael Cuesta Ruiz (2021)
 * ----

*/


var configService = function(configStorage){


  var self = this;

  self.configStorage = configStorage;


  /* Obtenemos el nombre del jugador A */
  self.getNameUserA = function()
  {
      var configWeb = self.getCurrentConfig()

      return configWeb.getNameUserA();
  }

  /* Obtenemos el nombre del jugador B */
  self.getNameUserB = function()
  {
      var configWeb = self.getCurrentConfig();
      return configWeb.getNameUserB();
  }


  /* Obtenemos la configuración actual */

  self.getCurrentConfig = function()
  {
      return self.configStorage.loadConfig();
  }

  /* Guardamos el nombre de los usuarios*/
  self.saveNameUsers = function(nameUserA, nameUserB)
  {

      var configWeb = new config(nameUserA, nameUserB);

      self.configStorage.saveConfig(configWeb);

  }


}
