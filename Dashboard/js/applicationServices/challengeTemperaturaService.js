/**
 * @fileoverview challengeTemperaturaService.js
 * Este servicio de retos, contiene toda la lógica de negocio necesaria para el reto de las posiciones
 * @version                               1.0
 *
 * @author                 Rafael Cuesta Ruiz (2021)
 * @copyright              Rafael Cuesta Ruiz (2021)
 * ----

*/


var challengeTemperaturaService = function(configService)
{

    var self=this;

    self.configService = configService;

    self.idCanvasUserA ='canvasMicrobitATemperatura';
    self.idCanvasUserB = 'canvasMicrobitBTemperatura';

    self.canvasUserA = document.getElementById(self.idCanvasUserA);
    self.canvasUserB = document.getElementById(self.idCanvasUserB);

    self.elapsedTime = 0;
    self.maxTime = parameters.secondsChallenge;

    self.tickTimer = null;

    self.callBackTimeLeft = null;
    self.callBackRankingAdd = null;
    self.callBackFirstTemperaturaUserA = null;
    self.callBackFirstTemperaturaUserB = null;

    self.isCompletedChallengeUserA = false;
    self.isCompletedChallengeUserB = false;

    /* Guardamos la primera temperatura registrada del jugadorA y del jugadorB */
    self.firstTemperaturaUserA = null;
    self.firstTemperaturaUserB = null;


    /* Este método inicia0. el reto, tiene unos "callbacks" para detectar cuando ocurren eventos, como que avance el tiempo,
    un jugador termine, o se equivoque
    */
    self.initChallenge = function(callBackTimeLeft, callBackRankingAdd, callBackFirstTemperaturaUserA, callBackFirstTemperaturaUserB)
    {

       self.callBackTimeLeft = callBackTimeLeft;
       self.callBackRankingAdd = callBackRankingAdd;
       self.callBackFirstTemperaturaUserA = callBackFirstTemperaturaUserA;
       self.callBackFirstTemperaturaUserB = callBackFirstTemperaturaUserB;


       self.isCompletedChallengeUserA = false;
       self.isCompletedChallengeUserB = false;

       self.firstTemperaturaUserA = null;
       self.firstTemperaturaUserB = null;


       if(self.callBackFirstTemperaturaUserA)
           self.callBackFirstTemperaturaUserA("?");

       if(self.callBackFirstTemperaturaUserB)
           self.callBackFirstTemperaturaUserB("?");

       self.startTimer();
       self.clearCanvasUserA();
       self.clearCanvasUserB();

    }

    /* Este tick se ejecuta cada segundo y disminuye la cuenta atras, y si ha expirado
    el tiempo máximo, dará por perdida la partida  */
    self.tick = function()
    {

      self.elapsedTime++;

      if(self.elapsedTime > self.maxTime){

      if(!self.isCompletedChallengeUserA)
        self.uncompletedChallengeUserA();

      if(!self.isCompletedChallengeUserB)
        self.uncompletedChallengeUserB();

      self.clearTimer();
      return;
    }

      var remainingSeconds = self.maxTime -self.elapsedTime;
      var timeLeft = self.getRemainingTime(remainingSeconds);

      if(self.callBackTimeLeft)
        self.callBackTimeLeft(timeLeft);

    }


    /* Este método nos devuelve el tiempo restante que queda en minutos y segundos mm:ss */
    self.getRemainingTime = function(remainingSeconds){


        minutes = Math.floor(remainingSeconds / 60);
        seconds = remainingSeconds - minutes * 60;

        var finalTime = self.getTwoDigits(minutes) + ':' + self.getTwoDigits(seconds);

        return finalTime;

    }

    /* Este método convierte un numero de un digito a dos, por ejemplo un 1 se devuelve como 01*/
    self.getTwoDigits = function (unit){

      return (unit < 10?'0':'') + unit;
    }

    /* Aquí se inicia el timer, para que se invoque el tick cada segundo (1000ms) */
    self.startTimer = function(){

       self.elapsedTime = 0;
       self.clearTimer();
       self.tickTimer = setInterval(self.tick, 1000);

    }

    /* Reseteamos el timer para que no se invoque más la cuenta atras */
    self.clearTimer = function(){
      if(self.tickTimer){
         console.log("He limpiado timer!");
         clearInterval(self.tickTimer);
      }

    }

    /* Limpiamos el lienzo del jugador A */
    self.clearCanvasUserA = function(){

      var context = self.canvasUserA.getContext('2d');
      context.clearRect(0,0, self.canvasUserA.width, self.canvasUserA.width);
    }

    /* Limpiamos el lienzo del jugador B */
    self.clearCanvasUserB = function(){

      var context = self.canvasUserB.getContext('2d');
      context.clearRect(0,0, self.canvasUserB.width, self.canvasUserB.width);

    }

     /* Notificamos en pantalla que el jugador A termino y lo volcamos en el ranking */

    self.completedChallengeUserA = function(){

        self.drawCompletedChallengeCanvasUserA();
        self.isCompletedChallengeUserA = true;

        if(self.callBackRankingAdd)
          self.callBackRankingAdd(self.configService.getNameUserA(),self.elapsedTime);


    }

     /* Notificamos en pantalla que el jugador B termino y lo volcamos en el ranking */

    self.completedChallengeUserB = function(){

      self.drawCompletedChallengeCanvasUserB();
      self.isCompletedChallengeUserB = true;

      if(self.callBackRankingAdd)
        self.callBackRankingAdd(self.configService.getNameUserB(),self.elapsedTime);

    }


    /* Notificamos que el jugadorA no ha completado el reto y lo volcamos en el ranking */

    self.uncompletedChallengeUserA = function(){

        self.drawUncompletedChallengeCanvasUserA();

        if(self.callBackRankingAdd)
          self.callBackRankingAdd(self.configService.getNameUserA(),null);

    }

   /* Notificamos que el jugadorB no ha completado el reto y lo volcamos en el ranking */

    self.uncompletedChallengeUserB = function(){

      self.drawUncompletedChallengeCanvasUserB();

      if(self.callBackRankingAdd)
        self.callBackRankingAdd(self.configService.getNameUserB(),null);

    }

    /* Pintamos en pantalla que el jugadorA no ha completado el reto*/

    self.drawUncompletedChallengeCanvasUserA = function(){
        self.clearCanvasUserA();
        self.writeCenterTextCanvasUserA("Reto incompleto :(");
    }



    /* Pintamos en pantalla que el jugadorB no ha completado el reto*/

    self.drawUncompletedChallengeCanvasUserB = function(){
      self.clearCanvasUserB();
      self.writeCenterTextCanvasUserB("Reto incompleto :(");
    }

    /* Pintamos en pantalla que el jugadorA ha completado el reto*/
    self.drawCompletedChallengeCanvasUserA = function(){
        self.clearCanvasUserA();
        self.writeCenterTextCanvasUserA("¡Reto conseguido!");
    }

    /* Pintamos en pantalla que el jugadorB ha completado el reto*/
    self.drawCompletedChallengeCanvasUserB = function(){
      self.clearCanvasUserB();
      self.writeCenterTextCanvasUserB("¡Reto conseguido!");
    }

    /* Escribimos un texto en el centro del liezno del jugadorA */
    self.writeCenterTextCanvasUserA = function(text){

      var context = self.canvasUserA.getContext('2d');
      context.fillStyle = "purple";
      context.font = "50px Arial";
      context.textAlign = "center";
      context.fillText(text,self.canvasUserB.width/2,self.canvasUserB.height/2);

    }

    /* Escribimos un texto en el centro del liezno del jugadorB */
    self.writeCenterTextCanvasUserB = function(text){

      var context = self.canvasUserB.getContext('2d');
      context.fillStyle = "blue";
      context.font = "50px Arial";
      context.textAlign = "center";
      context.fillText(text,self.canvasUserB.width/2,self.canvasUserB.height/2);


    }

    /* Cuando recibimos un mensaje, lo procesamos como el jugadorA o el jugador B */

    self.onMessage = function(nameUser, message)
    {
      if(nameUser=="A")
      {
        self.processMessageA(message);
      }

      else if(nameUser=="B")
      {
        self.processMessageB(message);
      }

    }

  /* Invoca a los métodos adecuados del jugadorA */
    self.processMessageA = function(message){

        if(message.endsWith("C")){

          self.clearCanvasUserA();

          var temperatura = message.slice(0,-1);

          if(self.firstTemperaturaUserA == null){
              self.firstTemperaturaUserA = parseInt(temperatura);

              if(self.callBackFirstTemperaturaUserA)
                self.callBackFirstTemperaturaUserA(temperatura);
          }


          else if((parseInt(temperatura) - self.firstTemperaturaUserA) >=3 ){

              if(self.tickTimer && !self.isCompletedChallengeUserA)
              {

                self.completedChallengeUserA();
                return;

              }

          }

          self.clearCanvasUserA();
          self.writeCenterTextCanvasUserA("Temperatura:"+ temperatura + "º C");


        }

        else if(message.endsWith("F")){

          var temperatura = message.slice(0,-1);

          self.clearCanvasUserA();
          self.writeCenterTextCanvasUserA("Temperatura:"+ temperatura + "º F");
        }
    }

  /* Invoca a los métodos adecuados del jugadorB */

    self.processMessageB = function(message){

        if(message.endsWith("C")){



          var temperatura = message.slice(0,-1);

          if(self.firstTemperaturaUserB==null){

               self.firstTemperaturaUserB=parseInt(temperatura);

               if(self.callBackFirstTemperaturaUserB)
                 self.callBackFirstTemperaturaUserB(temperatura);
           }


           else if((parseInt(temperatura) - self.firstTemperaturaUserB) >=3 ){

               if(self.tickTimer && !self.isCompletedChallengeUserB)
               {

                 self.completedChallengeUserB();
                 return;

               }

           }

          self.clearCanvasUserB();
          self.writeCenterTextCanvasUserB("Temperatura:"+ temperatura + "º C");

        }

        else if(message.endsWith("F")){


          var temperatura = message.slice(0,-1);

          self.clearCanvasUserB();
          self.writeCenterTextCanvasUserB("Temperatura:"+ temperatura + "º F");

        }


    }
}
