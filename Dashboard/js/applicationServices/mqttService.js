/**
 * @fileoverview 	mqttService.js
 *	Este componente se encarga de establecer una conexión con el mqtt. También se encarga de coordinar los mensajes e invocar
 *  al servicio correspondiente para ejecutar la lógica de negocio adecuada.
 *
 * @version                               1.0
 *
 * @author                 	Rafael Cuesta Ruiz (2021)
 * @copyright           	Rafael Cuesta Ruiz (2021)
 * ----

*/


var mqttService = function(challengeLetrasService, challengePosicionesService, challengeTemperaturaService){

  var self = this;

  /* Guardamos las referencias a los servicios de nuestros retos */

  self.challengeLetrasService = challengeLetrasService;
  self.challengePosicionesService = challengePosicionesService;
  self.challengeTemperaturaService = challengeTemperaturaService;

 /* La configuración de nuestro mqtt, host, puerto y el nombre del cliente javascript */
  self.hostname = "45.76.84.39";
  self.port = 8083;
  self.clientId = "webTfgRafa";
  self.clientId += new Date().getUTCMilliseconds();

  /* Monitorizamos todos estos topics, esto se hace porque el jugador A y el jugador B publicaran en dos tópicos distintos.*/
  self.subscription = "tfg/+/rafa";


  /* Jugador A, Jugador B, y el mensaje de cambiar de programa usan tópicos distintos*/

  self.nameTopicMicrobitA = "tfg/ma/rafa";
  self.nameTopicMicrobitB = "tfg/mb/rafa";
  self.nameTopicWeb = "tfg/web/rafa"

  self.mqttClient = null;



  // los programas en los que se encuentra la microbit
  // 0 - Letras
  // 1 - Temperatura
  // 2 - Posiciones


/* Los programas actuales seleccionados */
  self.indexProgramA = -1;
  self.indexProgramB = -1;


/* Callbacks para notificar eventos */

  self.callBackConnected = null;
  self.callBackProgramA = null;
  self.callBackProgramB = null;

  self.setCallBacks = function(callBackConnected, callBackProgramA, callBackProgramB){
     self.callBackConnected = callBackConnected;
     self.callBackProgramA = callBackProgramA;
     self.callBackProgramB = callBackProgramB;
  }

/* Este método estable una conexión con el mqtt */
  self.connect = function(){


    self.mqttClient = new Paho.MQTT.Client(self.hostname, self.port, self.clientId);
    self.mqttClient.onMessageArrived =  self.messageArrived;
    self.mqttClient.onConnectionLost = self.connectionLost;

    self.mqttClient.connect({
  		onSuccess: self.connected,
  		onFailure: self.connectionFailed,
  		keepAliveInterval: 10,
  		useSSL: false,
  	});

  }


/* Este evento ocurre cuando nos hemos conectado correctamente*/
  self.connected = function()
  {

     self.mqttClient.subscribe(self.subscription)
     console.log("Estoy conectado!!!!");

     if(self.callBackConnected)
         self.callBackConnected(true);

  }

/* Este evento ocurre cuando la conexion ha fallado */
  self.connectionFailed = function(res)
  {

      console.log("Connect failed:" + res.errorMessage);
      if(self.callBackConnected)
          self.callBackConnected(false);

  }

  /* Este evento ocurre cuando la conexión se ha perdido*/
  self.connectionLost = function(res)
  {
      if (res.errorCode != 0)
      {
          console.log("Connection lost:" + res.errorMessage);
          if(self.callBackConnected)
              self.callBackConnected(false);
          setTimeout(self.connect,3000);


      }
  }


/* Cuando recibimos un mensaje lo recibimos aqui y detectamos si procede del jugadorA, o del B y actuamos acorde a ello*/
  self.messageArrived = function(message)
  {

      var topic = message.destinationName;

      if(topic == self.nameTopicMicrobitA ){

          /* Notificamos que estamos usando este programa si es necesario */
          self.changeIndexProgramIfNeededMicrobitA(message.payloadString);
          self.sendMessageToProperChallengeServiceA(message.payloadString);

      }

      else if(topic == self.nameTopicMicrobitB){

         /* Notificamos que estamos usando este programa si es necesario */
         self.changeIndexProgramIfNeededMicrobitB(message.payloadString);
         self.sendMessageToProperChallengeServiceB(message.payloadString);

      }

  	  console.log(message.destinationName +" : " + message.payloadString);
  }


/* Este método coge un mensaje y lo invoca al servicio de retos adecuado para el jugadorA*/
  self.sendMessageToProperChallengeServiceA = function(messageRaw){

     if(self.indexProgramA == 0){
         self.challengeLetrasService.onMessage("A",messageRaw);
     }

     else if(self.indexProgramA == 1){
        self.challengeTemperaturaService.onMessage("A",messageRaw);
     }

     else if(self.indexProgramA == 2){
       self.challengePosicionesService.onMessage("A",messageRaw);
     }


     else{
       console.log("No puedo mandar mensaje!");
     }

  }

/* Este método detecta a traves del ping en que programa esta la Micro:bit del jugadorB */
  self.sendMessageToProperChallengeServiceB = function(messageRaw){

    if(self.indexProgramB == 0){
        self.challengeLetrasService.onMessage("B",messageRaw);
    }

    else if(self.indexProgramB == 1){
      self.challengeTemperaturaService.onMessage("B",messageRaw);
    }

    else if(self.indexProgramB ==2){
      self.challengePosicionesService.onMessage("B",messageRaw);
    }

    else{
      console.log("No puedo mandar mensaje!");
    }


  }


/* Este método detecta a traves del ping en que programa esta la Micro:bit del jugadorA */
  self.changeIndexProgramIfNeededMicrobitA = function(messageRaw)
  {
      if(messageRaw === "PING_A" && self.indexProgramA!=0)
      {
          self.indexProgramA=0;
          console.log("MicrobitA ahora esta en el programa A!");

          if(self.callBackProgramA)
              self.callBackProgramA("Letras");
      }
      else if(messageRaw === "PING_B" && self.indexProgramA!=1)
      {
          self.indexProgramA=1;
          console.log("MicrobitA ahora esta en el programa B!");

          if(self.callBackProgramA)
              self.callBackProgramA("Temperatura");
      }
      else if(messageRaw == "PING_C" && self.indexProgramA!=2)
      {
          self.indexProgramA=2;
          console.log("MicrobitA ahora esta en el programa C!");

          if(self.callBackProgramA)
              self.callBackProgramA("Posiciones");
      }

  }

/* Este método detecta a traves del ping en que programa esta la Micro:bit del jugadoB */
  self.changeIndexProgramIfNeededMicrobitB = function(messageRaw)
  {
      if(messageRaw === "PING_A" && self.indexProgramB!=0)
      {
          self.indexProgramB=0;
          console.log("MicrobitB ahora esta en el programa A!");

          if(self.callBackProgramB)
              self.callBackProgramB("Letras");
      }
      else if(messageRaw === "PING_B" && self.indexProgramB!=1)
      {
          self.indexProgramB=1;
          console.log("MicrobitB ahora esta en el programa B!");

          if(self.callBackProgramB)
              self.callBackProgramB("Temperatura");
      }
      else if(messageRaw == "PING_C" && self.indexProgramB!=2)
      {
          self.indexProgramB=2;
          console.log("MicrobitB ahora esta en el programa C!");

          if(self.callBackProgramB)
              self.callBackProgramB("Posiciones");
      }

  }


/* Enviamos un mensaje a un determinado tópico */
  self.sendMessage = function(message){

      console.log("Voy a enviar mensaje:"+ message);
      self.mqttClient.send(self.nameTopicWeb,message,0, false);

  }



}
