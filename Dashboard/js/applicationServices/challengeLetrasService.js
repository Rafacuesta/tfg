
/**
 * @fileoverview challengeLetrasService.js
 * Este servicio de retos, contiene toda la logica de negocio necesaria para el reto de las letras
 *
 * @version                               1.0
 *
 * @author                 	Rafael Cuesta Ruiz (2021)
 * @copyright           	Rafael Cuesta Ruiz (2021)
 * ----

*/


var challengeLetrasService = function(configService){


  var self=this;

  self.configService = configService;

  self.idCanvasUserA ='canvasMicrobitALetras';
  self.idCanvasUserB = 'canvasMicrobitBLetras';

  self.canvasUserA = document.getElementById(self.idCanvasUserA);
  self.canvasUserB = document.getElementById(self.idCanvasUserB);

  self.elapsedTime = 0;
  self.maxTime = parameters.secondsChallenge;

  self.tickTimer = null;

  self.callBackTimeLeft = null;
  self.callBackRankingAdd = null;
  self.callBackFailedA = null;
  self.callBackFailedB = null;

  self.isCompletedChallengeUserA = false;
  self.isCompletedChallengeUserB = false;


/* Este metodo inicita el reto, tiene unos "callbacks" para detectar cuando ocurren eventos, como que avance el tiempo,
un jugador termine, o se equivoque
*/
  self.initChallenge = function(callBackTimeLeft, callBackRankingAdd, callBackFailedA, callBackFailedB)
  {

     self.callBackTimeLeft = callBackTimeLeft;
     self.callBackRankingAdd = callBackRankingAdd;
     self.callBackFailedA = callBackFailedA;
     self.callBackFailedB = callBackFailedB;

     self.isCompletedChallengeUserA = false;
     self.isCompletedChallengeUserB = false;

     /* Como acabo de empezar , no nos hemos equivocado aun en ningun letra */

     if(self.callBackFailedA)
         self.callBackFailedA("?");

     if(self.callBackFailedB)
         self.callBackFailedB("?");


     self.startTimer();
     self.clearCanvasUserA();
     self.clearCanvasUserB();

  }

/* Este tick se ejecuta cada segundo y disminuye la cuenta atras, y si ha expirado el tiempo maximo, dará por perdida la partida
*/
  self.tick = function()
  {

    self.elapsedTime++;

    if(self.elapsedTime > self.maxTime){

    if(!self.isCompletedChallengeUserA)
      self.uncompletedChallengeUserA();

    if(!self.isCompletedChallengeUserB)
      self.uncompletedChallengeUserB();

    self.clearTimer();
    return;
  }

    var remainingSeconds = self.maxTime -self.elapsedTime;
    var timeLeft = self.getRemainingTime(remainingSeconds);

    if(self.callBackTimeLeft)
      self.callBackTimeLeft(timeLeft);

  }

/* Este método nos devuelve el tiempo restante que queda en minutos y segundos mm:ss */
  self.getRemainingTime = function(remainingSeconds){


      minutes = Math.floor(remainingSeconds / 60);
      seconds = remainingSeconds - minutes * 60;

      var finalTime = self.getTwoDigits(minutes) + ':' + self.getTwoDigits(seconds);

      return finalTime;

  }

/* Este método convierte un numero de un digito a dos, por ejemplo un 1 se devuelve como 01*/
  self.getTwoDigits = function (unit){

    return (unit < 10?'0':'') + unit;
  }


/* Aquí se inicia el timer, para que se invoque el tick cada segundo (1000ms)*/
  self.startTimer = function(){

     self.elapsedTime = 0;
     self.clearTimer();
     self.tickTimer = setInterval(self.tick, 1000);

  }


/* Reseteamos el timer para que no se invoque más la cuenta atras */
  self.clearTimer = function(){
    if(self.tickTimer){
       console.log("He limpiado timer!");
       clearInterval(self.tickTimer);
    }

  }


/* Limpiamos el lienzo del jugador A */
  self.clearCanvasUserA = function(){

    var context = self.canvasUserA.getContext('2d');
    context.clearRect(0,0, self.canvasUserA.width, self.canvasUserA.width);
  }

/* Limpiamos el liezno del jugador B */
  self.clearCanvasUserB = function(){

    var context = self.canvasUserB.getContext('2d');
    context.clearRect(0,0, self.canvasUserB.width, self.canvasUserB.width);

  }

  /* Notificamos en pantalla que el jugador A termino y lo volcamos en el ranking*/

  self.completedChallengeUserA = function(){

      self.drawCompletedChallengeCanvasUserA();
      self.isCompletedChallengeUserA = true;

      if(self.callBackRankingAdd)
        self.callBackRankingAdd(self.configService.getNameUserA(),self.elapsedTime);


  }

  /* Notificamos en pantalla que el jugador B termino y lo volcamos en el ranking */

  self.completedChallengeUserB = function(){

    self.drawCompletedChallengeCanvasUserB();

      self.isCompletedChallengeUserB = true;
    if(self.callBackRankingAdd)
      self.callBackRankingAdd(self.configService.getNameUserB(),self.elapsedTime);

  }


  /* Notificamos que el jugadorA no ha completado el reto y lo volcamos en el ranking */

  self.uncompletedChallengeUserA = function(){

      self.drawUncompletedChallengeCanvasUserA();

      if(self.callBackRankingAdd)
        self.callBackRankingAdd(self.configService.getNameUserA(),null);

  }

  /* Notificamos que el jugadorB no ha completado el reto y lo volcamos en el ranking */
  self.uncompletedChallengeUserB = function(){

    self.drawUncompletedChallengeCanvasUserB();

    if(self.callBackRankingAdd)
      self.callBackRankingAdd(self.configService.getNameUserB(),null);

  }


/* Pintamos en pantalla que el jugadorA no ha completado el reto*/

  self.drawUncompletedChallengeCanvasUserA = function(){
      self.clearCanvasUserA();
      self.writeCenterTextCanvasUserA("Reto incompleto :(");
  }


/* Pintamos en pantalla que el jugadorB no ha completado el reto*/

  self.drawUncompletedChallengeCanvasUserB = function(){
    self.clearCanvasUserB();
    self.writeCenterTextCanvasUserB("Reto incompleto :(");
  }


/* Pintamos en pantalla que el jugadorA ha completado el reto*/
  self.drawCompletedChallengeCanvasUserA = function(){
      self.clearCanvasUserA();
      self.writeCenterTextCanvasUserA("¡Reto conseguido!");
  }


/* Pintamos en pantalla que el jugadoB ha completado el reto*/

  self.drawCompletedChallengeCanvasUserB = function(){
    self.clearCanvasUserB();
    self.writeCenterTextCanvasUserB("¡Reto conseguido!");
  }


/* Escribimos un texto en el centro del liezno del jugadorA */
  self.writeCenterTextCanvasUserA = function(text){

    var context = self.canvasUserA.getContext('2d');
    context.fillStyle = "purple";
    context.font = "50px Arial";
    context.textAlign = "center";
    context.fillText(text,self.canvasUserB.width/2,self.canvasUserB.height/2);

  }


/* Escribimos un texto en el centro del liezno del jugadorB */
  self.writeCenterTextCanvasUserB = function(text){

    var context = self.canvasUserB.getContext('2d');
    context.fillStyle = "blue";
    context.font = "50px Arial";
    context.textAlign = "center";
    context.fillText(text,self.canvasUserB.width/2,self.canvasUserB.height/2);


  }


/* Cuando recibimos un mensaje, lo procesamos como el jugadorA o el jugador B */

  self.onMessage = function(nameUser, message){

      if(nameUser=="A")
        self.processMessageA(message);

      else if(nameUser=="B")
        self.processMessageB(message);

  }


/* Invoca a los metodos adecuados del jugadorA */
  self.processMessageA = function(message)
  {
      console.log("Mensaje de microbitA en letras:" + message);

      //hemos recibido una letra actual
      if(message.startsWith("A"))
      {
          self.clearCanvasUserA();
          self.writeCenterTextCanvasUserA(message.substring(1));
      }

      else if(message.startsWith("L"))
      {

        self.clearCanvasUserA();
        self.writeCenterTextCanvasUserA("Nivel " + message.substring(1));

        /* reto cumplido */
        if(message === "L3")
        {

          if(self.tickTimer && !self.isCompletedChallengeUserA)
          {
            self.completedChallengeUserA();
          }

        }

      }
      /* game over */
      else if(message.startsWith("GO"))
      {
        self.clearCanvasUserA();
        self.writeCenterTextCanvasUserA("FALLASTE :(");

        if(self.callBackFailedA){
          self.callBackFailedA(message.substring(2));
        }
      }


     else if(message.startsWith("S"))
     {
       self.clearCanvasUserA();
       self.writeCenterTextCanvasUserA("CORRECTAS:" + message.substring(1));
     }


  }


/* Invoca a los metodos adecuados del jugadorB */

  self.processMessageB = function(message)
  {
      console.log("Mensaje de microbitB en letras:" + message);

       /* ha llegado una letra actual */
      if(message.startsWith("A"))
      {
          self.clearCanvasUserB();
          self.writeCenterTextCanvasUserB(message.substring(1));
      }


       /* ha llegado el nivel actual */
      else if(message.startsWith("L"))
      {
        self.clearCanvasUserB();
        self.writeCenterTextCanvasUserB("Nivel " + message.substring(1));

          /* RETO CUMPLIDO */
        if(message === "L3")
        {

          if(self.tickTimer && !self.isCompletedChallengeUserB)
          {
            self.completedChallengeUserB();
          }

        }

      }


      /* ha llegado la letra en la que perdiste */
      else if(message.startsWith("GO"))
      {
        self.clearCanvasUserB();
        self.writeCenterTextCanvasUserB("FALLASTE :(");


        if(self.callBackFailedB){
          self.callBackFailedB(message.substring(2));
        }


      }


     else if(message.startsWith("S"))
     {
       self.clearCanvasUserB();
       self.writeCenterTextCanvasUserB("CORRECTAS:" + message.substring(1));
     }


  }


}
