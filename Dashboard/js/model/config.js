/**
 * @fileoverview 	config.js
 *	Este modelo representa la configuración de la aplicacion que se persistirá.
 *  Necesitamos guardar el nombre del jugadorA y el nombre del jugadorB
 *
 * @version                               1.0
 *
 * @author                 	Rafael Cuesta Ruiz (2021)
 * @copyright           	Rafael Cuesta Ruiz (2021)
 * ----

*/


var config = function(nameUserA, nameUserB){

  var self = this;

  self.nameUserA = nameUserA;

  self.nameUserB = nameUserB;

  /* Devuelve el nombre del jugador A*/
  self.getNameUserA = function(){
    return self.nameUserA;
  }

  /* Devuelve el nombre del jugador B */
  self.getNameUserB = function(){
    return self.nameUserB;
  }


}
