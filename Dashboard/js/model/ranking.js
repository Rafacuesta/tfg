/**
 * @fileoverview 	rankings.js
 *	Este modelo representa un ranking, internamente guarda una serie de líneas, y cada vez que se añade una
 *  las reordena garantizando así que siempre se mantengan ordenadas de mayor a menor.
 *
 * @version                               1.0
 *
 * @author                 	Rafael Cuesta Ruiz (2021)
 * @copyright           	Rafael Cuesta Ruiz (2021)
 * ----

*/


var ranking = function(){

  var self = this;

  // Todas las lineas del ranking
  self.lines = [];

 /* Añàde una nueva linea de ranking y las ordena*/
  self.addRankingLine = function(rankingLine){

      self.lines.push(rankingLine);
      self.sortLines();

  }

  /* Ordena las lineas de ranking */
  self.sortLines = function(){

    self.lines.sort(function(a,b)
    {

      if (a.timeCompletedInSeconds === null){
        return 1;
      }

      else if (b.timeCompletedInSeconds === null){
        return -1;
      }

      else {
          return a.timeCompletedInSeconds - b.timeCompletedInSeconds;
      }



    });
  }


}
