/**
 * @fileoverview 	parameters.js
 *	Parámetros globables de la configuración
 *
 * @version                               1.0
 *
 * @author                 	Rafael Cuesta Ruiz (2021)
 * @copyright           	Rafael Cuesta Ruiz (2021)
 * ----

*/


var parameters = new function(){

  var self = this;
  self.secondsChallenge = 120;

}
